import { store } from "../index";
import { IAction, EActions } from "../reducers";

const RESThost = "http://localhost:3000";

/**
 * Ask to the backend for the hotels with the given filters and the given sorting field.
 * When the frontend gets the response, it alters the Redux store.
 * @param filters Which filters to send.
 * @param sortBy Which sorting method we ask for.
 */
export function getHotelsAndAlterStore(filters: any = {}, sortBy: string = "UserRating") {
  // Build the query from filters and sortBy
  let query = "?sortBy=" + sortBy;
  for (const k in filters) {
    query += "&filter" + k + "=" + JSON.stringify(filters[k]);
  }

  const request = new XMLHttpRequest();
  request.addEventListener("load", (event) => {
    const hotels = JSON.parse(request.responseText);
    store.dispatch<IAction>({type: EActions.UPDATE_VISIBLE_HOTELS, payload: hotels});
  });
  request.open("GET", RESThost + query);
  request.send();
}