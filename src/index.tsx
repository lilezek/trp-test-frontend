import * as React from "react";
import * as Redux from "redux";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { createStore } from "redux";
import hotelListApp, {defaultStore} from "./reducers";
import App from "./components/app";
import { getHotelsAndAlterStore } from "./api/API";

// Redux store with debugging enabled:
export const store = createStore(hotelListApp, (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__());

// Redux store without debug:
// const store = createStore(hotelListApp);

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app-root")
)

// We call to getHotelsAndAlterStore first to populate the initial webpage:
getHotelsAndAlterStore([]);