import * as React from "react";
import { connect } from "react-redux";
import FilterToolbar from "./filtertoolbar";
import SortingToolbar from "./sortingtoolbar";
import { Hotels } from "./hotels";

/**
 * Application element. Contains the whole website.
 */
export default class App extends React.Component<{}, {}> {
  render() {
    return <div>
      <Hotels />
      <div className="CloseToolbar" onClick={(e) => this.closeToolbar(e)}></div>
      <div className="Toolbar">
        <div className="OpenToolbar"><button onClick={(e) => this.openToolbar(e)}>Filter and sort by</button></div>
        <FilterToolbar />
        <SortingToolbar />
      </div>
    </div>;
  }

  private openToolbar(e: React.SyntheticEvent<HTMLButtonElement>) {
    const toolbar = document.querySelector(".Toolbar") as HTMLDivElement;
    const close = document.querySelector(".CloseToolbar") as HTMLDivElement;
    
    toolbar.classList.add("ForceShow");
    close.classList.add("ForceShow");
  }

  private closeToolbar(e: React.SyntheticEvent<HTMLDivElement>) {
    const toolbar = document.querySelector(".Toolbar") as HTMLDivElement;
    const close = document.querySelector(".CloseToolbar") as HTMLDivElement;

    toolbar.classList.remove("ForceShow");
    close.classList.remove("ForceShow");
  }
}