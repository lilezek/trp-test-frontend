import * as React from "react";
import { connect } from "react-redux";

interface IHotel {
  index: number;
  Distance: number;
  EstablishmentId: number;
  EstablishmentType: string;
  Location: string;
  MinCost: number;
  Name: string;
  Stars: number;
  UserRating: number;
  UserRatingTitle: string;
  UserRatingCount: number;
  ImageUrl: string;
  ThumbnailUrl: string;
}

interface IHotelProps {
  hotelList: IHotel[];
  sortBy: string;
}

/**
 * Rount to first digit.
 */
const round = (num: number) => ((num * 10) | 0) / 10;

/**
 * Hotels list. Connected to redux state.
 */
class HotelsElement extends React.Component<IHotelProps, {}> {
  render() {
    // Render the list of the hotels:
    const hotels = this.props.hotelList.slice().sort((a,b) => (a[this.props.sortBy] - b[this.props.sortBy])).map((v) => {
      const stars = ("★" as any).repeat(v.Stars);
      return <div className="Hotel" key={v.EstablishmentId}>
        <label>{v.Name}</label>
        <img className="Thumbnail" src={v.ThumbnailUrl} /> 
        {/* <img className="Thumbnail" src="http://www.brandsoftheworld.com/sites/default/files/styles/logo-thumbnail/public/042011/youtube_logo.png" />   */}
        <div className="Establishment">{v.EstablishmentType} <span className="Stars">{stars}</span></div>
        <div className="Distance">Distance: {round(v.Distance)}</div>
        <div className="Location">{v.Location}</div>
        <div className="Price">£{v.MinCost}</div>
        <div className="UserRating">User rating: {v.UserRating}</div>
      </div>
    });
    return <div className="Hotels">
      {...hotels}
    </div>;
  }
}

const HotelsGenerator = (props: IHotelProps) => <HotelsElement {...props} />;

export const Hotels = connect((state, ownProps) => {
  return {
    hotelList: state.visibleHotels,
    sortBy: state.toolbar.sortBy,
  }
})(HotelsGenerator);

