import * as React from "react";
import { connect } from "react-redux";
import { EActions } from "../reducers";

/**
 * Props of sorting toolbar.
 */
interface ISortingToolbarProps {
  onChange: (changedTo: string) => void;
}

/**
 * Sorting toolbar element.
 */
class SortingToolbarElement extends React.Component<ISortingToolbarProps, {}> {
  render() {
    return <div>
      Sort by: <br />
      <input onClick={(e) => this.props.onChange(e.currentTarget.value)} type="radio" name="sortBy" value="Distance" /> Distance <br />
      <input onClick={(e) => this.props.onChange(e.currentTarget.value)} type="radio" name="sortBy" value="UserRating" /> User rating <br />
      <input onClick={(e) => this.props.onChange(e.currentTarget.value)} type="radio" name="sortBy" value="Stars" /> Stars <br />
      <input onClick={(e) => this.props.onChange(e.currentTarget.value)} type="radio" name="sortBy" value="MinCost" /> Cost <br />
    </div>;
  }
}

const SortingToolbarGenerator = (props: ISortingToolbarProps) => (<SortingToolbarElement {...props} />);

const SortingToolbar = connect(
  null,
  (dispatch) => {
    return {
      onChange: (changedTo) => dispatch({
        type: EActions.CHANGE_SORTBY,
        payload: changedTo,
      })
    }
  }
)(SortingToolbarGenerator);

export default SortingToolbar;