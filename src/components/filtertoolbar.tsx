import * as React from "react";
import { connect } from "react-redux";
import { EActions } from "../reducers";

/**
 * Props of FilterToolbar
 */
interface IFilterToolbarProps {
  onChange: (newFilters: any) => void;
}

/**
 * Filter toolbar element.
 */
export class FilterToolbarElement extends React.Component<IFilterToolbarProps, {}> {
  // Generate random id. Useful for using querySelector.
  private uniqueID = ("id"+Math.random()).replace(".","");

  render() {
    return <div id={this.uniqueID} className="FilterToolbar">
      Filter by: <br />
      <div> <label>Name:</label> <span className="Name"><input type="text" /></span> </div> <br />
      <div> <label>Stars:</label> <span className="Stars"><input placeholder="Min" type="number"  min="0" max="5"/> <input placeholder="Max" type="number"  min="0" max="5"/></span> </div> <br />
      <div> <label>User Rating:</label> <span className="UserRating"><input placeholder="Min" type="number" min="0" max="10"/> <input placeholder="Max" type="number" min="0" max="10"/></span> </div> <br />
      <div> <label>Cost:</label> <span className="MinCost"><input placeholder="Min" type="number" min="0" max="100000" /> <input placeholder="Max" type="number" min="0" max="100000"/></span> </div> <br />
      <button onClick={(e) => this.onChange(e)}>Apply filters</button>
    </div>;
  }

  private onChange(e: React.SyntheticEvent<HTMLButtonElement>) {
    const filters = {} as any;
    // Iterate over spans
    const spans = document.querySelectorAll("#"+this.uniqueID + " span");
    for (let i = 0; i < spans.length; i++) {
      const span = spans[i] as HTMLSpanElement;
      const inputs = span.querySelectorAll("input");
      // Check if span have className Name
      if (span.classList.contains("Name")) {
        // Only filter if the input is not empty:
        if (inputs[0].value !== "") {
          filters["Name"] = [inputs[0].value];
        }
      } else {
        // If the rest of spans, add the range:
        if (inputs[0].value !== "" && inputs[1].value !== "") {
          // The range is an array of two values as numbers:
          filters[span.className] = [inputs[0].value, inputs[1].value].map(parseFloat);
        }
      }
    }
    // Now we have all filters, we pass it to the callback
    this.props.onChange(filters);
  }
}

const FilterToolbarGenerator = (props: IFilterToolbarProps) => (<FilterToolbarElement {...props} />);

const FilterToolbar = connect(
  null,
  (dispatch) => ({
    onChange: (newFilters) => dispatch({
      type: EActions.CHANGE_FILTERS,
      payload: newFilters
    }) 
  })
)(FilterToolbarGenerator);

export default FilterToolbar;
