import { combineReducers } from "redux";
import { getHotelsAndAlterStore } from "./api/API";

export enum EActions {
  UPDATE_VISIBLE_HOTELS = "UPDATE_VISIBLE_HOTELS",
  CHANGE_SORTBY = "CHANGE_SORTBY",
  CHANGE_FILTERS = "CHANGE_FILTER",
}

export interface IAction {
  type: EActions;
  payload?: any;
}

export interface IHotelListState {
  visibleHotels: any[];
  toolbar: {
    sortBy: string;
    filterBy: any[];
  }
}

export const defaultStore = {
  visibleHotels: [],
  toolbar: {
    sortBy: "Stars",
    filterBy: {},
  }
}

export function visibleHotelsReducer(state: any[] = [], action: IAction) {
  switch (action.type) {
    case (EActions.UPDATE_VISIBLE_HOTELS):
      return action.payload;
    default:
      return state;
  }
}

let lastSortBy = "Stars";
export function sortByReducer(state: string = "Stars", action: IAction) {
  switch (action.type) {
    case (EActions.CHANGE_SORTBY):
      lastSortBy = action.payload;
      return action.payload;
    default:
      return state;
  }
}

export function filterByReducer(state: any = {}, action: IAction) {
  if (action.type === EActions.CHANGE_FILTERS) {
    // When filters change, update the dataset from backend:
    getHotelsAndAlterStore(action.payload, lastSortBy);
    return action.payload;
  } else {
    return state;
  }
}


const combined = combineReducers({
  visibleHotels: visibleHotelsReducer,
  toolbar: combineReducers({
    sortBy: sortByReducer,
    filterBy: filterByReducer,
  })
})

export default combined;